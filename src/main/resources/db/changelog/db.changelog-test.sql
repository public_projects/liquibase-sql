--liquibase formatted sql

--changeset nvoxland:1
DROP TABLE IF EXISTS `RELATIONSHIP`;

--changeset nvoxland:2
DROP TABLE IF EXISTS `MEMBERS`;

--changeset nvoxland:3
CREATE TABLE if not exists MEMBERS(
  id   INT,
  name VARCHAR(50)
);

--changeset nvoxland:4
CREATE TABLE if not exists RELATIONSHIP (
  MEM_ID     INT,
  PAR_MEM_ID INT
);

--changeset nvoxland:5
INSERT INTO self_joining_db.MEMBERS
VALUES
  (1, 'a1'),
  (2, 'a1.1'),
  (3, 'a1.1.1'),
  (4, 'a1.1.1.1'),
  (5, 'a1.1.1.2'),
  (6, 'a1.1.1.1.1'),
  (7, 'a2'),
  (8, 'a2.1');

INSERT INTO self_joining_db.RELATIONSHIP
VALUES
  (6, 4),
  (5, 3),
  (4, 3),
  (3, 2),
  (2, 1);