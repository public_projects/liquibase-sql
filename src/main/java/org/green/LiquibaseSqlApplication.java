package org.green;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquibaseSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquibaseSqlApplication.class, args);
	}
}
